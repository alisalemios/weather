//
//  CitiesListView.swift
//  WeatherApp
//
//  Created by Ali Salem on 21/12/2020.
//

import Foundation
import UIKit
import SDWebImage

class CitiesListView: UIViewController {
  
    // MARK: - Outlets
    
    @IBOutlet var txtField: UITextField!
    @IBOutlet var table: UITableView!
    @IBOutlet var viewAlert: UIView!
    @IBOutlet var lblStatus: UILabel!
    @IBOutlet var constLblStatus: NSLayoutConstraint!
    
    // MARK: - Properties
        
    var DATA = DataClass()
    var isCached = false // for checking the data comes from API || DB
    var cityName = ""
    
    var arrayOfData: [WeatherModel] = [] {
        didSet {
            table.reloadData()
        }
    }
    
    var currentWeather: WeatherModel? {
        didSet {
        }
    }

    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - Get data
    
    func getDataByCityName(_ cityName: String) {

        if arrayOfData.count > 0 {
            arrayOfData.removeAll()
        }
     
        showError()
        let arr = UtilitiesDB.shared.getWeathers()
        
        guard let model2 = arr.first (where: {cityName == $0.name }) else { return }
        arrayOfData.append(model2)
        table.reloadData()
        hideError()
        checkStatus()
    }
    
    func getDataAPI(_ cityName: String ) {
        LOAD.show(self)
        DATA.getData(cityName) { (model, success) in
            if let model = model, success {
                self.currentWeather = model
                self.arrayOfData.append(model)
                self.isCached = false
                self.hideError()
                self.checkStatus()
                let model = CityModel(cityName, model.weather.first!.icon, model.weather.first!.main , "\(model.main.temp)")
                UtilitiesDB.shared.saveCity(model)
            }else{
                print(success)
                self.isCached = true
                self.getDataByCityName(cityName)
            }
        }
    }
    
    // MARK: - Show/Hide Error/Status
    
    func checkStatus() {
        if isCached {
            constLblStatus.constant = 40
        }else{
            constLblStatus.constant = 0
        }
    }
    
    func showError() {
        table.isHidden = true
        viewAlert.isHidden = false
    }
    
    func hideError() {
        table.isHidden = false
        viewAlert.isHidden = true
    }

    // MARK: - Search
    
    @IBAction func search(_ sender: Any) {
        getDataAPI(txtField.text!)
        cityName = txtField.text!
        txtField.text = ""
    }
    
    @IBAction func retry(_ sender: Any) {
        print(cityName)
        getDataAPI(cityName)
    }
}

    // MARK: - Extension

extension CitiesListView: UITableViewDataSource, UITableViewDelegate {
  
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CellWeather
        let model = arrayOfData[indexPath.row]
        cell.setData(model)
        
        return cell
    }
        
}
