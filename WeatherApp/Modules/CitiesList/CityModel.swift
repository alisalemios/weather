//
//  CityModel.swift
//  WeatherApp
//
//  Created by Ali Salem on 21/12/2020.
//

import Foundation
import UIKit
import CoreData

struct CityModel: Codable {
    var weathers: [WeatherModel]
    var name: String
    var icon: String
    var desc: String
    var temp: String

    func getManagedObject(_ context: NSManagedObjectContext) -> CityEntity {
        let object = CityEntity(context: context)
        
        object.name = name
        object.desc = desc
        object.icon = icon
        object.temp = temp
        object.weathers = NSSet.init(array: weathers.map{ $0.getManagedObject(context) })
        
        return object
    }
    
    init(_ mName: String,_ mIcon: String,_ mDesc: String,_ mTemp: String) {
        name = mName
        icon = mIcon
        desc = mDesc
        temp = mTemp
        weathers = []
    }
    
    init(_ object: CityEntity) {
        name = object.name ?? ""
        desc = object.desc ?? ""
        temp = object.temp ?? ""
        icon = object.icon ?? ""

        weathers = ((object.weathers?.allObjects as? [WeathersEntity] ?? [])).map{ WeatherModel($0) }
    }
}
