//
//  CityWeatherViewModel.swift
//  WeatherApp
//
//  Created by Ali Salem on 21/12/2020.
//

import Foundation

class DataClass: NSObject {
    
    func getData(_ city: String, _ handler: @escaping (WeatherModel?, Bool)->()) {
      
        let parameters = ["q": city]
             
        APISERVICE.callAPI(MethodType.GET, apiUrl: API.Weather, parameters: parameters) { (response, responseObject, error) in
//        API_WEATHER.callAPI(type: MethodType.GET, apiUrl: API.Weather, parameters: parameters) { (isDone, responseObject) in
            LOAD.close {
                if let json = responseObject {
                    do {
                        let data = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                        var weather = try JSONDecoder().decode(WeatherModel.self, from: data)
                        weather.date = Date()
                        UtilitiesDB.shared.saveWeather(weather)
                        
                        handler(weather, true)
                    } catch {
                        handler(nil, false)
                    }
                } else {
                    handler(nil, false)
                }
            }
        }
    }
    
}
