//
//  WeatherModel.swift
//  WeatherApp
//
//  Created by Ali Salem on 21/12/2020.
//

import Foundation
import UIKit
import CoreData

struct WeatherModel: Codable {
    var weather: [Weather]
    var main: Main
    var wind: Wind
    var base: String
    var visibility: Double
    var dt: Double
    var timezone: Double
    var id: Int64
    var name: String
    var cod: Double
    var date: Date?
    
    func getManagedObject(_ context: NSManagedObjectContext) -> WeathersEntity {
        let object = WeathersEntity(context: context)
        object.base = base
        object.visibility = visibility
        object.dt = dt
        object.timezone = timezone
        object.id = id
        object.name = name
        object.cod = cod
        object.date = date
        object.main = main.getManagedObject(context)
        object.wind = wind.getManagedObject(context)
        object.weather = NSSet.init(array: weather.map{ $0.getManagedObject(context) })
        return object
    }
    
    init(_ object: WeathersEntity) {
        base = object.base ?? ""
        visibility = object.visibility
        dt = object.dt
        timezone = object.timezone
        id = object.id
        name = object.name ?? ""
        cod = object.cod
        date = object.date
        main = Main(object.main ?? MainEntity())
        wind = Wind(object.wind ?? WindEntity())
        weather = ((object.weather?.allObjects as? [WeatherEntity] ?? [])).map{ Weather($0) }
    }
}

struct Weather: Codable {
    var id: Int64
    var main: String
    var icon: String
    
    func getManagedObject(_ context: NSManagedObjectContext) -> WeatherEntity {
        let weatherObject = WeatherEntity(context: context)
        weatherObject.id = id
        weatherObject.main = main
        weatherObject.icon = icon
        return weatherObject
    }
    
    init(_ object: WeatherEntity) {
        id = object.id
        main = object.main ?? ""
        icon = object.icon ?? ""
    }
}

struct Main: Codable {
    var temp: Double
    var feels_like: Double
    var temp_min: Double
    var temp_max: Double
    var pressure: Double
    var humidity: Double
    
    func getManagedObject(_ context: NSManagedObjectContext) -> MainEntity {
        let mainObject = MainEntity(context: context)
        mainObject.temp = temp
        mainObject.feels_like = feels_like
        mainObject.temp_min = temp_min
        mainObject.temp_max = temp_max
        mainObject.pressure = pressure
        mainObject.humidity = humidity
        return mainObject
    }
    
    init(_ object: MainEntity) {
        temp = object.temp
        feels_like = object.feels_like
        temp_min = object.temp_min
        temp_max = object.temp_max
        pressure = object.pressure
        humidity = object.humidity
    }
}

struct Wind: Codable {
    var speed: Double
    var deg: Double
    
    func getManagedObject(_ context: NSManagedObjectContext) -> WindEntity {
        let windObject = WindEntity(context: context)
        windObject.speed = speed
        windObject.deg = deg
        return windObject
    }
    
    init(_ object: WindEntity) {
        speed = object.speed
        deg = object.deg
    }
}
