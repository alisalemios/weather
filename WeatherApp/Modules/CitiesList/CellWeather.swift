//
//  CellWeather.swift
//  WeatherApp
//
//  Created by Ali on 7/30/21.
//

import UIKit

class CellWeather: UITableViewCell {

    @IBOutlet var img: UIImageView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblDescription: UILabel!
    @IBOutlet var lblTemperature: UILabel!
    @IBOutlet var lblStatus: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func setData(_ model: WeatherModel) {
        img.sd_setImage(with: URL(string: String(format: API.ImageURL, model.weather.first?.icon ?? "")), completed: nil)
        lblName.text = model.name
        lblDescription.text = model.weather.first?.main ?? ""
        lblTemperature.text = "\(Int((model.main.temp ) - kelvinDiff))° C"
    }

}
