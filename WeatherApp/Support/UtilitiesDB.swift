//
//  UtilitiesDB.swift
//  WeatherApp
//
//  Created by Ali Salem on 21/12/2020.
//

import UIKit
import CoreData

class UtilitiesDB: NSObject {

    static var shared = UtilitiesDB()
    
    override init() {
        super.init()
    }
}

extension UtilitiesDB {
    func saveCity(_ model: CityModel) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let context = appDelegate.persistentContainer.viewContext
        _ = model.getManagedObject(context)
        
        do {
            try context.save()
            print("saved")
        } catch {
            print("saved error")
        }
    }
    
    func getCities() -> [CityModel] {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return [] }
        let context = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<CityEntity>(entityName: "CityEntity")
        
        do {
            let results = try context.fetch(fetchRequest)
            return results.map{ CityModel($0) }
        } catch  {
            print("could not retrieve")
        }
        
        return []
    }
    
    func saveWeather(_ model: WeatherModel) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let context = appDelegate.persistentContainer.viewContext
        _ = model.getManagedObject(context)
        
        do {
            try context.save()
            print("saved")
        } catch {
            print("saved error")
        }
    }
    
    func getWeathers() -> [WeatherModel] {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return [] }
        let context = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<WeathersEntity>(entityName: "WeathersEntity")
        
        do {
            let results = try context.fetch(fetchRequest)
            return results.map{ WeatherModel($0) }
        } catch  {
            print("could not retrieve")
        }
        return []
    }
    
}
