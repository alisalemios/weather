//
//  APIService.h
//  WeatherApp
//
//  Created by Ali Salem on 21/12/2020.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class API, MethodType;

@interface APIService : NSObject {}

+ (instancetype)sharedInstance;

- (void)callAPI:(NSString *)method apiUrl:(NSString *)apiUrl parameters:(NSDictionary *)parameters completionHandler:(nullable void (^)(NSURLResponse *response, id _Nullable responseObject,  NSError * _Nullable error))completionHandler;
- (void)callAPI:(NSString *)baseURL method:(NSString *)method apiUrl:(NSString *)apiUrl parameters:(NSDictionary *)parameters completionHandler:(nullable void (^)(NSURLResponse *response, id _Nullable responseObject,  NSError * _Nullable error))completionHandler;
    
@end

NS_ASSUME_NONNULL_END
