//
//  APIService.m
//  WeatherApp
//
//  Created by Ali Salem on 21/12/2020.
//

#import "APIService.h"
#import "AFNetworking.h"
#import "WeatherApp-Swift.h"

@implementation APIService

+ (instancetype)sharedInstance {
    static APIService *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[APIService alloc] init];
    });
    return sharedInstance;
}

- (void)callAPI:(NSString *)method apiUrl:(NSString *)apiUrl parameters:(NSDictionary *)parameters completionHandler:(nullable void (^)(NSURLResponse *response, id _Nullable responseObject,  NSError * _Nullable error))completionHandler {
    [self callAPI:API.BaseURL method:method apiUrl:apiUrl parameters:parameters completionHandler:completionHandler];
    
}

- (void)callAPI:(NSString *)baseURL method:(NSString *)method apiUrl:(NSString *)apiUrl parameters:(NSDictionary *)parameters completionHandler:(nullable void (^)(NSURLResponse *response, id _Nullable responseObject,  NSError * _Nullable error))completionHandler {
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@", baseURL, apiUrl];
    
    NSMutableDictionary *mutableParameters = [NSMutableDictionary dictionaryWithDictionary:parameters];
    [mutableParameters setValue:@"f5cb0b965ea1564c50c6f1b74534d823" forKey:@"appid"];
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:method URLString:urlString parameters:mutableParameters error:nil];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request uploadProgress:nil downloadProgress:nil completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        if (error) {
            NSLog(@"Error: %@", error);
        } else {
            NSLog(@"%@ %@", response, responseObject);
        }
        
        completionHandler(response, responseObject, error);
    }];
    [dataTask resume];
}

@end
