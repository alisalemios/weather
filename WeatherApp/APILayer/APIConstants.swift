//
//  APIConstants.swift
//  WeatherApp
//
//  Created by Ali Salem on 21/12/2020.
//

import Foundation

let APISERVICE = APIService.sharedInstance()

@objcMembers class API: NSObject {
    static let APIKey = "f5cb0b965ea1564c50c6f1b74534d823"
    static let BaseURL = "https://api.openweathermap.org/data/2.5/"
    static let ImageURL = "https://openweathermap.org/img/w/%@.png"
    static let Weather = "weather"
}

@objcMembers class MethodType: NSObject {
    static let GET = "GET"
    static let POST = "POST"
}
