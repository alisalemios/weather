//
//  APIWeather.swift
//  WeatherApp
//
//  Created by Ali Salem on 21/12/2020.
//

import Foundation
import AFNetworking

class APIWeather: NSObject {
   
    static var shared = APIWeather()
    
    func callAPI (type: String, apiUrl: String, parameters: Any?, _ completionHandler: @escaping (_ isDone: Bool, _ responceObject: Any) -> Void) {
        
        // Create a URLRequest for an API endpoint
        let url = URL(string: "\(API.BaseURL)\(apiUrl)")!
        var request = URLRequest(url: url)
        
        // Configure request authentication
        request.setValue("authToken", forHTTPHeaderField: "Authorization")
        request.setValue("f5cb0b965ea1564c50c6f1b74534d823", forHTTPHeaderField: "appid")
        
        // Serialize HTTP Body data as JSON
        let body = parameters
        let bodyData = try? JSONSerialization.data(
            withJSONObject: body!,
            options: []
        )
        
        // Change the URLRequest to a POST request
        request.httpMethod = type
        request.httpBody = bodyData
        
        // Create the HTTP request
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (data, response, error) in
            print(response as Any)
            if error != nil {
                // Handle HTTP request error
            } else if data != nil {
                // Handle HTTP request response
            } else {
                // Handle unexpected error
            }
        }
        task.resume()
    }
    
}
