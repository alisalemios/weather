//
//  WindEntity+CoreDataProperties.swift
//  WeatherApp
//
//  Created by Ali Salem on 21/12/2020.
//
//

import Foundation
import CoreData


extension WindEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<WindEntity> {
        return NSFetchRequest<WindEntity>(entityName: "WindEntity")
    }

    @NSManaged public var deg: Double
    @NSManaged public var speed: Double
    @NSManaged public var toWeathers: WeathersEntity?

}
