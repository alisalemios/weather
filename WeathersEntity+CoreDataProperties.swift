//
//  WeathersEntity+CoreDataProperties.swift
//  WeatherApp
//
//  Created by Ali Salem on 21/12/2020.
//
//

import Foundation
import CoreData


extension WeathersEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<WeathersEntity> {
        return NSFetchRequest<WeathersEntity>(entityName: "WeathersEntity")
    }

    @NSManaged public var base: String?
    @NSManaged public var visibility: Double
    @NSManaged public var dt: Double
    @NSManaged public var timezone: Double
    @NSManaged public var id: Int64
    @NSManaged public var name: String?
    @NSManaged public var cod: Double
    @NSManaged public var date: Date?
    @NSManaged public var toCity: CityEntity?
    @NSManaged public var main: MainEntity?
    @NSManaged public var wind: WindEntity?
    @NSManaged public var weather: NSSet?

}

// MARK: Generated accessors for weather
extension WeathersEntity {

    @objc(addWeatherObject:)
    @NSManaged public func addToWeather(_ value: WeatherEntity)

    @objc(removeWeatherObject:)
    @NSManaged public func removeFromWeather(_ value: WeatherEntity)

    @objc(addWeather:)
    @NSManaged public func addToWeather(_ values: NSSet)

    @objc(removeWeather:)
    @NSManaged public func removeFromWeather(_ values: NSSet)

}
