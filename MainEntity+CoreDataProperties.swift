//
//  MainEntity+CoreDataProperties.swift
//  WeatherApp
//
//  Created by Ali Salem on 21/12/2020.
//
//

import Foundation
import CoreData


extension MainEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<MainEntity> {
        return NSFetchRequest<MainEntity>(entityName: "MainEntity")
    }

    @NSManaged public var feels_like: Double
    @NSManaged public var humidity: Double
    @NSManaged public var pressure: Double
    @NSManaged public var temp: Double
    @NSManaged public var temp_max: Double
    @NSManaged public var temp_min: Double
    @NSManaged public var toWeathers: WeathersEntity?

}
