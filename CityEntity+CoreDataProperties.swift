//
//  CityEntity+CoreDataProperties.swift
//  WeatherApp
//
//  Created by Ali Salem on 21/12/2020.
//
//

import Foundation
import CoreData


extension CityEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CityEntity> {
        return NSFetchRequest<CityEntity>(entityName: "CityEntity")
    }

    @NSManaged public var name: String?
    @NSManaged public var icon: String?
    @NSManaged public var desc: String?
    @NSManaged public var temp: String?
    @NSManaged public var weathers: NSSet?

}

// MARK: Generated accessors for weathers
extension CityEntity {

    @objc(addWeathersObject:)
    @NSManaged public func addToWeathers(_ value: WeathersEntity)

    @objc(removeWeathersObject:)
    @NSManaged public func removeFromWeathers(_ value: WeathersEntity)

    @objc(addWeathers:)
    @NSManaged public func addToWeathers(_ values: NSSet)

    @objc(removeWeathers:)
    @NSManaged public func removeFromWeathers(_ values: NSSet)

}
