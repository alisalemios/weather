//
//  WeatherEntity+CoreDataProperties.swift
//  WeatherApp
//
//  Created by Ali Salem on 21/12/2020.
//
//

import Foundation
import CoreData


extension WeatherEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<WeatherEntity> {
        return NSFetchRequest<WeatherEntity>(entityName: "WeatherEntity")
    }

    @NSManaged public var icon: String?
    @NSManaged public var id: Int64
    @NSManaged public var main: String?
    @NSManaged public var toWeathers: WeathersEntity?

}
